"use strict";

const { MoleculerClientError } = require("moleculer").Errors;
const normalizeEmail = require("normalize-email");
const jwt = require('jsonwebtoken');
const bcrypt = require("bcrypt");
const DbService = require("moleculer-db");

module.exports = {
	name: "users",
	mixins: [DbService],
	settings: {
		fields: ["_id", "username", "email"],
		secret: process.env.AUTH_SECRET || "dev-auth-secret"
	},
	actions: {
		register: {
			params: {
				username: "string",
				email: "email",
				password: "string"
			},
			async handler(ctx) {
				const { username, password } = ctx.params;
				const email = normalizeEmail(ctx.params.email);
				const passwordHash = await bcrypt.hash(password, 10);

				{
					const user = await this.adapter.findOne({ username	});
					if (user) {
						throw new MoleculerClientError("Username is already taken!");
					}
				}

				{
					const user = await this.adapter.findOne({ email });
					if (user) {
						throw new MoleculerClientError("Email is already taken!");
					}
				}

				await this.adapter.insert({
					username,
					email,
					password: passwordHash
				});
			}
		},
		login: {
			params: {
				login: "string",
				password: "string"
			},
			async handler(ctx) {
				const { login, password } = ctx.params;

				const user = await this.adapter.findOne({ username: login })
					.then(user => {
						if (user) {
							return user;
						} else {
							const email = normalizeEmail(login);
							return this.adapter.findOne({ email });
						}
					});

				if (!user || !(await bcrypt.compare(password, user.password))) {
					throw new MoleculerClientError("Username (email) or password are incorrect!");
				}

				const token = jwt.sign({
					id: user._id,
					username: user.username,
					email: user.email
				}, this.settings.secret);

				return { token };
			}
		},
		resolveToken: {
			params: {
				token: "string"
			},
			async handler(ctx) {
				const { token } = ctx.params;

				let doc;
				try {
					doc = jwt.verify(token, this.settings.secret);
				} catch (error) {
					return null;
				}

				if (doc) {
					const user = await this.adapter.findOne({ _id: doc.id });
					if (!user) {
						return null;
					}

					return this.transformDocuments(ctx, {}, user);
				} else {
					return null;
				}
			}
		},
		hello: {
			auth: "required",
			handler: (ctx) => `Hello, ${ctx.meta.user.username}`
		}
	}
};