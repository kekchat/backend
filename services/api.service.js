"use strict";

const ApiGateway = require("moleculer-web");
const { MoleculerClientError } = require("moleculer").Errors;

module.exports = {
	name: "api",
	mixins: [ApiGateway],

	settings: {
		port: process.env.PORT || 3000,

		routes: [{
			path: "/api",
			authorization: true,
			aliases: {
				"POST register": "users.register",
				"POST login": "users.login",
				"GET hello": "users.hello"
			},
			mappingPolicy: "restrict"
		}],

		assets: {
			folder: "public"
		}
	},

	methods: {
		async authorize(ctx, route, req) {
			const required = req.$endpoint.action.auth === "required";

			const authorization = req.headers.authorization;
			if (!authorization) {
				if (required) {
					throw new MoleculerClientError("Not Authorized", 403);
				} else {
					return ctx;
				}
			}
			if (!authorization.startsWith("Bearer ")) {
				throw new MoleculerClientError("Invalid Authorization header!", 403);
			}

			const token = ctx.meta.token = authorization.slice(7);
			const user = ctx.meta.user = await this.broker.call("users.resolveToken", { token });

			if (required && !user) {
				throw new MoleculerClientError("Not Authorized", 403);
			}

			return ctx;
		},
	}
};